'use strict';

/**
 * @ngdoc overview
 * @name exotechniaApp
 * @description
 * # exotechniaApp
 *
 * Main module of the application.
 */
angular
  .module('exotechniaApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap'
  ]);


angular.module('exotechniaApp').config(function($stateProvider,$urlRouterProvider,$httpProvider){
	$httpProvider.defaults.useXDomain = true;
	$httpProvider.defaults.withCredentials = true;
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
	$httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";

    



	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('home',{
			url : '/',
			templateUrl : 'views/main.html'
		})
		.state('playlists',{
			url : '/playlists',
			controller : 'PlaylistCtrl',
			controllerAs : 'playlists',
			templateUrl : 'views/playlists.html'
		})

		.state('artists',{
			url : '/artists',
			controller : 'ArtistsCtrl',
			controllerAs : 'artists',
			templateUrl : 'views/artists.html'
		})

		.state('play',{
			url : '/play/?artist',
			templateUrl : 'views/play.html',
			controller : 'PlayCtrl',
			controllerAs : 'play'
		})

		.state('events',{
			url : '/events',
			templateUrl : 'views/events.html',
			controller : 'EventsCtrl',
			controllerAs : 'events'
		})

		.state('event',{
			url : '/events/:eventName',
			templateUrl : 'views/event.html',
			controller : 'EventCtrl',
			controllerAs : 'event'
		})

		.state('sync',{
			url : '/sync',
			templateUrl : 'views/sync.html'
		});


	SC.initialize({
		client_id: 'ec2d77f447b4095ad998815e20f4d76d'
	});


});