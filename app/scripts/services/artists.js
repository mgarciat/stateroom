'use strict';

/**
 * @ngdoc service
 * @name exotechniaApp.artists
 * @description
 * # Artists
 * Factory in the exotechniaApp.
 */
angular.module('exotechniaApp')
  .factory('Artists', function ($resource) {
    
    return $resource('http://localhost:8080/artists/:name',{ },{
      post : {
        method : 'POST',
        isArray:false,
        params : { name : '@name'}
      },
      get : {
        method : 'GET',
        isArray:false,
        params : {}
      },
      headers : {'Content-Type' : 'application/json'}

    });
    
  });
