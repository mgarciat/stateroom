'use strict';

/**
 * @ngdoc service
 * @name exotechniaApp.Labels
 * @description
 * # Labels
 * Service in the exotechniaApp.
 */
angular.module('exotechniaApp')
  .service('Labels', function ($resource) {
    return $resource('http://localhost:8080/labels/:id',{ id : '@_id' },{
      POST : {
        method : 'POST',
        isArray:false
      },
      GET : {
        method : 'GET',
        isArray:false
      },
      headers : {'Content-Type' : 'application/x-www-form-urlencoded'}

    });
    
  });
