'use strict';

/**
 * @ngdoc service
 * @name exotechniaApp.Playlist
 * @description
 * # Playlist
 * Factory in the exotechniaApp.
 */
angular.module('exotechniaApp')
  .factory('Playlist', function ($resource) {
    
    return $resource('http://localhost:8080/playlists/:id',{ id : '@_id' },{
      POST : {
        method : 'POST',
        isArray:false
      },
      GET : {
        method : 'GET',
        isArray:false
      },
      headers : {'Content-Type' : 'application/x-www-form-urlencoded'}

    });
    
  });
