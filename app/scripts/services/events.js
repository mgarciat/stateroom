'use strict';

/**
 * @ngdoc service
 * @name exotechniaApp.Events
 * @description
 * # Events
 * Factory in the exotechniaApp.
 */
angular.module('exotechniaApp')
  .factory('Events', function ($resource) {
    // Service logic
    // ...

    return $resource('http://localhost:8080/events/:name',{ },{
      post : {
        method : 'POST',
        isArray:false,
        params : { name : '@name'}
      },
      get : {
        method : 'GET',
        isArray:false,
        params : { name : '@name'}
      },
      headers : {'Content-Type' : 'application/json'}

    });
  });
