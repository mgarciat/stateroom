'use strict';

/**
 * @ngdoc function
 * @name exotechniaApp.controller:PlaylistCtrl
 * @description
 * # PlaylistCtrl
 * Controller of the exotechniaApp
 */

var playlistCtrl = function ($modal,$sce,Playlist) {
	var vm = this;
    vm.artists = [];
    
    Playlist.get(function(data){
    	console.log("completed");
    	vm.artists = data;
        console.log(data);
    });

    vm.open = function(song){
    	$modal.open({
    		 templateUrl: 'views/youtube-modal.html',
    		 controllerAs : 'modal',
    		 controller : function(){
    		 	var vm = this;
    		 	vm.song = song;
    		 	vm.youtubeUrl = $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + song.youtubeUrl);
    		 }
    	});

        

    };

    vm.save = function(){
    	Playlist.save(vm.playlistId,function(){
    		console.log("saved");
	    	console.log(Playlist);
	    	vm.playlistId = "";	
    	},function(){
    		console.log("error saving");
    	});
    	
    };


};

angular.module('exotechniaApp')
  .controller('PlaylistCtrl', playlistCtrl);