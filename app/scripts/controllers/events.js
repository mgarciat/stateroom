'use strict';

/**
 * @ngdoc function
 * @name exotechniaApp.controller:EventsCtrl
 * @description
 * # EventsCtrl
 * Controller of the exotechniaApp
 */

var EventsCtrl = function (Events) {
    var vm = this;
    vm.events = {};

    Events.query(function(events){
    	vm.events = events;
    });

  }

angular.module('exotechniaApp')
  .controller('EventsCtrl', EventsCtrl);
