'use strict';
/*global SC:false */

/**
 * @ngdoc function
 * @name exotechniaApp.controller:PlayCtrl
 * @description
 * # PlayCtrl
 * Controller of the exotechniaApp
 */

 var PlayCtrl = function (Artists,Playlist,$stateParams,$sce) {
    var vm = this;

   	vm.artist = {};
    vm.playlists = {};

   	Artists.get({name : $stateParams.artist },function(artist){
      vm.artist = artist;
      vm.artist.embedUrl =  $sce.trustAsResourceUrl("https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/users/" + artist.soundcloudUrl  +"&amp;auto_play=true&amp;hide_related=true&amp;show_comments=false&amp;show_user=true&amp;show_reposts=false&amp;visual=true");
      console.log("fetched");
      

    });

    Playlist.get({name : $stateParams.artist },function(playlists){
      vm.playlists = playlists;
      
      

    });


 };

angular.module('exotechniaApp')
  .controller('PlayCtrl', PlayCtrl);
