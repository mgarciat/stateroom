'use strict';

/**
 * @ngdoc function
 * @name exotechniaApp.controller:ArtistsCtrl
 * @description
 * # ArtistsCtrl
 * Controller of the exotechniaApp
 */

var artistsCtrl = function(Artists,Labels){
	var vm = this;
	vm.artists = [];

	Artists.query(function(data){
		vm.artists = data;
	});

	Labels.query(function(data){
		vm.labels = data;
	});

	vm.save = function(){
		Artists.save(vm.new,function(){
			console.log("artist saved");
			vm.artists[vm.artists.length] = vm.new;
			console.log(vm.new);
		});
	};


	
};

angular.module('exotechniaApp')
  .controller('ArtistsCtrl', artistsCtrl);
