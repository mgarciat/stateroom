'use strict';

/**
 * @ngdoc function
 * @name exotechniaApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the exotechniaApp
 */
angular.module('exotechniaApp')
  .controller('MainCtrl', function () {
    this.artists = [
      'Tale Of Us',
      'Maceo Plex',
      'Solomun',
      'Thugfucker'
    ];
  });
