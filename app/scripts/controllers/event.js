'use strict';

/**
 * @ngdoc function
 * @name exotechniaApp.controller:EventCtrl
 * @description
 * # EventCtrl
 * Controller of the exotechniaApp
 */
var EventCtrl = function (Events,$stateParams) {
    var vm = this;
    vm.currentEvent = {};

    Events.get({name : $stateParams.eventName },function(event){
    	vm.currentEvent = event;
    	console.log(event);
    });
  };

angular.module('exotechniaApp')
  .controller('EventCtrl', EventCtrl);
