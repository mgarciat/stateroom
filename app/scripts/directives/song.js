'use strict';

/**
 * @ngdoc directive
 * @name exotechniaApp.directive:song
 * @description
 * # song
 */
angular.module('exotechniaApp')
  .directive('song', function () {
    return {
      templateUrl: 'views/song.html',
      restrict: 'E'
    };
  });
