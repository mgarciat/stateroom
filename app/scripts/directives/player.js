'use strict';

/**
 * @ngdoc directive
 * @name exotechniaApp.directive:player
 * @description
 * # player
 */
angular.module('exotechniaApp')
  .directive('player', function () {
    return {
      templateUrl: 'views/player.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
      },
      controller : function(){
      	var vm = this;

      	vm.isPlaying = false;
      	vm.currentArtist = "Maceo Plex";
      	vm.currentTrack = "Heads Above";

      	vm.play = function(){

      	}

      	vm.stop = function(){

      	}

      },
      controllerAs : "player"
    };
  });
