'use strict';

/**
 * @ngdoc directive
 * @name exotechniaApp.directive:sidebar
 * @description
 * # sidebar
 */
angular.module('exotechniaApp')
  .directive('sidebar', function () {
    return {
      templateUrl: 'views/sidebar.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        var el = element;
		var trigger = angular.element(angular.element.find('.hamburger')),
		overlay = angular.element(angular.element.find('.overlay')),
		isClosed = false;

		trigger.click(function () {
			hamburger_cross();      
		});

		function hamburger_cross() {

			if (isClosed === true) {          
				overlay.hide();
				trigger.removeClass('is-open');
				trigger.addClass('is-closed');
				isClosed = false;
			} else {   
				overlay.show();
				trigger.removeClass('is-closed');
				trigger.addClass('is-open');
				isClosed = true;
			}
		}
		
		angular.element(angular.element.find('.toggler')).bind('click',function () {
		    angular.element(angular.element.find('#wrapper')).toggleClass('toggled');
		});  
      }
    };
  });
