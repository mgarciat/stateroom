'use strict';
/*global SC:false */

/**
 * @ngdoc directive
 * @name exotechniaApp.directive:artist
 * @description
 * # artist
 */
angular.module('exotechniaApp')
  .directive('artist', function () {
    return {
      templateUrl: 'views/artist.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {

      },
      controller: function($state){
      	var vm = this;
      	vm.isHovering = false;
      	
      	vm.toggleOverlay = function(){
      		console.log("Toggle");
			     vm.isHovering = !vm.isHovering;
		    };

        vm.play = function(artistInfo){
          $state.go('play',{'artist' : artistInfo.name});
        };

      },
      controllerAs : 'artistSquare'
    };
  });
