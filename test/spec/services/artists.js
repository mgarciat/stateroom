'use strict';

describe('Service: artists', function () {

  // load the service's module
  beforeEach(module('exotechniaApp'));

  // instantiate service
  var artists;
  beforeEach(inject(function (_artists_) {
    artists = _artists_;
  }));

  it('should do something', function () {
    expect(!!artists).toBe(true);
  });

});
